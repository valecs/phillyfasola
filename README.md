# phillyfasola.bot.nu
Site source and systemd service for managing the [Jamulus](https://sourceforge.net/projects/llcon/) server at <phillyfasola.bot.nu>

## Notes on hosting (Nov. 6, 2020)
Jamulus is largely CPU bound and until recently had no multi-core support. We use [Vultr's](https://www.vultr.com) cheapest (6 USD/month) "High Frequency" VPS because of its higher single clock speed. I have not yet needed to experiment with any of their multi-core offerings.

That server's specs:
* 3792 MHz CPU (`cat /proc/cpuinfo`)
* 1G of RAM (way more than necessary)
* 1000G of monthly bandwidth

We get about 150 singer-hours out of 1GB of bandwidth. Our server has successfully supported 15 simultaneous active singers without any issues. We'd love to go higher, but it takes considerable time/perseverance to get people set up---this has been the bottleneck in all the communities I've worked with.

The most important feature I've found in using a reputable host like Vultr is a close (few hops) connection to the internet backbone. We used a smaller provider that was physically closer to us but the network topography was such that latency was *worse*.

I use [Arch Linux](https://www.archlinux.org/) with a fairly minimal environment because I use it for everything I can and because other users have a [jamulus package](https://aur.archlinux.org/packages/jamulus-headless/) that integrates into my workflow on Arch.
