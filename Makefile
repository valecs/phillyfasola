# These directives should not generally be edited as there exist
# special sudo rules to allow them

PUB := /srv/http/

all: site service

service: /etc/systemd/system/jamulus.service /etc/jamulus/welcome.html /etc/jamulus/jamulusrc
	sudo systemctl restart jamulus.service

/etc/jamulus/welcome.html: resources/welcome_philly.html
	sudo install -m 0644 -DT $< $@

/etc/jamulus/jamulusrc: resources/jamulusrc
	install -m 0744 -DT $< $@

/etc/systemd/system/jamulus.service: resources/jamulus.service
	install -m 0644 -T $< $@
	systemctl daemon-reload

site:
	rsync --delete --recursive public/ ${PUB}
